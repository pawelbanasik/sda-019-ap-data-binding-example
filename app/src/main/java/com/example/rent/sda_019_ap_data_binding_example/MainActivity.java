package com.example.rent.sda_019_ap_data_binding_example;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rent.sda_019_ap_data_binding_example.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // skasowame pola
        private ActivityMainBinding binding;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // podmiana
            binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
            binding.setUser(new User("Pawel", 20));
            binding.nextUserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.setUser(new User("Adam", 18));
                }
            });

        }

//        private void bindUser(User user) {
//            binding.nameTextView.setText(user.getName());
//            binding.ageTextView.setText(String.valueOf(user.getAge()));
//        }
    }
